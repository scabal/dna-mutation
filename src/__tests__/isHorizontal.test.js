var isHorizontal = require('../utils/isHorizontal'); 

describe('isHorizontal', () => {

    it('Valida string sin mutacion "ATGCGA"', () => {
        expect(isHorizontal('ATGCGA')).toBe(false);
    });

    it('Valida string con mutacion "ACCCCA"', () => {
        expect(isHorizontal('ACCCCA')).toBe(true);
    });

    it('Valida string sin mutacion "CTCCCA"', () => {
        expect(isHorizontal('CTCCCA')).toBe(false);
    });

    it('Valida string con mutacion "CCCCCC"', () => {
        expect(isHorizontal('CCCCCC')).toBe(true);
    });

    it('Valida cadena sin mutacion ["ATGCGA","ATGCGA"]', () => {
        expect(isHorizontal(["ATGCGA","ATGCGA"])).toBe(false);
    });

    it('Valida cadena con mutacion ["ATGCGA","ACCCCA"]', () => {
        expect(isHorizontal(["ATGCGA","ACCCCA"])).toBe(true);
    });

})