
var dna = require('./utils/mutation');
var isValid = require('./utils/isValid');
var isHorizontal = require('./utils/isHorizontal'); 
var isVertical = require('./utils/isVertical'); 
var isOblique = require('./utils/isOblique'); 

const hasMutation = (dna) => {

    let response = false;

    const resIsValid = isValid(dna);
    const resHorizontal = isHorizontal(dna);
    const resVertical = isVertical(dna);
    const resOblique = isOblique(dna);

    if(!resIsValid) throw "Los caracteres solo pueden ser: A, T, C, G";

    if(resHorizontal || resVertical || resOblique ){
        response = true
    }

    console.log((response ? "tiene mutacion" : "no tiene mutacion"));
    return response
}

hasMutation(dna);