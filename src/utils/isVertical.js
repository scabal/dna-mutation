
//Verifica si existe alguna mutacion vertical en el modelo de adn
//Recibe el array de datos del modelo de adn
//Retorna true o false
function verticalMutation(dna) {
    let response = false;
    
    if( dna.length >=4 ){

        let repElement = [];
        //Itera cada renglon del modelo
        for(let i = 0; i < dna.length; i++){
            //Pasa por todos los caracteres y realiza una comprobacion de igualdad entre renglones
            for(let j = 0; j < dna[i].length; j++){
                if (i<dna.length-1 && dna[i].charAt(j) == dna[i+1].charAt(j)) {
                    repElement[dna[i].charAt(j)+(j+1)] = (repElement[dna[i].charAt(j)+(j+1)] || 1) + 1
                }
                
            }
        } 
        // console.log(repElement);
        
        //Busca a travez de los objetos del array de caracteres repetidos un valor buscado
        Object.filter = (obj, predicate) => 
            Object.keys(obj)
            .filter( key => predicate(obj[key]) )
            .reduce( (res, key) => (res[key] = obj[key], res), {} );

        //Selecciona todos aquellos objetos que se hayan repetido igual o mas de 4 veces. Retorna un objeto
        let filtered = Object.filter(repElement, element => element >= 4); 
        var result = Object.entries(filtered); //Transforma el objeto sin especificar a un array de datos para poderlo tratar

        if ( result.length > 0 ) {
            response = true
        }
    }

    return response;
}

function isVertical(dna) {

    let response = verticalMutation(dna)

    // console.log(response + " Vertical mutation");
    return response
}

module.exports = isVertical;