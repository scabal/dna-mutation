function invalidHorizontal(element, index, array) {
    let response = false;

    if(element.length >=4){
        let aux = element.charAt(0).toUpperCase();
        let count = 1;
        for(let i = 0; i < element.length; i++){
            if (aux === element.charAt(i+1).toUpperCase()) {
                count += 1;
            }else if(count<4){
                aux = element.charAt(i+1).toUpperCase()
                count = 1;
            }
        }
        // console.log(element + " " + count);

        if (count >= 4) {
            response = true
        }
    }
    // else throw "los elementos deben de ser de mas de 4 caracteres"

    return response;
}



function isHorizontal(dna) {

    let response
    if (typeof dna === 'string') {
        response = invalidHorizontal(dna)
    } else {
        response = (dna.find(invalidHorizontal) ? true : false);
    }
    // console.log(response + " horizontal mutation");
    return response
}

module.exports = isHorizontal;