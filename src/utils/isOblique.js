//Verifica si existe alguna mutacion oblicua en el modelo de adn
//Recibe el array de datos del modelo de adn
//Retorna true o false
function obliqueMutation(dna) {

    let response = false;
    if( dna.length >=4 && dna[0].length > 4){

        let count = 1;
        let aux = "";

        for (let i = 1 - dna[0].length; i < dna.length; i++){
            for (let j = -((0 < i) ? 0 : i), k = ((0 > i) ? 0 : i); j < dna[0].length && k < dna.length; j++, k++){

                if(aux === dna[k].charAt(j)) {
                    count += 1;
                } else {
                    if (count >= 4) {
                        break
                    } else {
                        count = 1;
                        aux = dna[k].charAt(j)
                    }
                }
            }

            if (count >= 4) {
                response = true
            } else {
                count = 1;
                aux = ""
            }
        }
    
    }

    return response;
}

function isOblique(dna) {

    let response = obliqueMutation(dna)

    // console.log(response + " Oblique mutation");
    return response
}

module.exports = isOblique;