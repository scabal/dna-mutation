# Mutacion de ADN

## Este programa valida si existe alguna mutacion en una secuencia de ADN

Recibe como parámetro un arreglo de cadena caracteres que representan cada fila de una tabla de NxN
con la secuencia del ADN. Las letras de los caracteres solo pueden ser: A, T, C, G; las cuales representan
cada base nitrogenada del ADN.

### Sin mutacion

```
[A][T][G][C][G][A]
[C][A][G][T][G][C]
[T][T][A][T][T][T]
[A][G][A][A][G][G]
[G][C][G][C][T][A]
[T][C][A][C][T][G]
```
### Con mutacion

```
[A][T][G][C][G][A]
[C][A][G][T][G][C]
[T][T][A][T][G][T]
[A][G][A][A][G][G]
[C][C][C][C][T][A]
[T][C][A][C][T][G]
```

Sabrás si existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua,
horizontal o vertical.

Ejemplo de caso con mutación:

```javascript
let dna = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA","TCACTG"];
```

---
# Instalación

Clonar el repositorio

ingresar la secuencia de ADN que deseas verificar
- la secuencia se puede modificar desde 'src/utils/mutation'

```javascript
module.exports = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA","TCACTG"];
```

---
# Uso

Solo necesitas modificar la secuencia y al ejecutar el comando

```
npm start
```


te mostrara como resultado: true o false dependiendo de si cuenta con mutación o no la cadena


